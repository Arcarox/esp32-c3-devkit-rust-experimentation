use esp_idf_svc::{eventloop::EspSystemEventLoop};
use esp_idf_sys as _; // If using the `binstart` feature of `esp-idf-sys`, always keep this module imported
use esp_idf_hal::{prelude::Peripherals, delay::FreeRtos, gpio::PinDriver};
use embedded_svc::ipv4;
use anyhow::Result;

mod wifi;

const SSID: &str = env!("RUST_ESP32_STD_DEMO_WIFI_SSID");
const PASS: &str = env!("RUST_ESP32_STD_DEMO_WIFI_PASS");

fn main() -> Result<()> {
    // It is necessary to call this function once. Otherwise some patches to the runtime
    // implemented by esp-idf-sys might not link properly. See https://github.com/esp-rs/esp-idf-template/issues/71
    esp_idf_sys::link_patches();

    // Bind the log crate to the ESP Logging facilities
    esp_idf_svc::log::EspLogger::initialize_default();

    let peripherals = Peripherals::take().unwrap();
    
    let sysloop = EspSystemEventLoop::take().unwrap();

    #[allow(unused)]
    let mut wifi = wifi::wifi(peripherals.modem, sysloop.clone(), SSID, PASS)?;
    let wifiStatusLock = wifi::start_wifi_controller_loop(wifi, sysloop.clone());

    // tcp::test_tcp_bind()?;

    // let _sntp = sntp::EspSntp::new_default()?;
    // info!("SNTP initialized");

    // #[allow(unused)]
    // let mqtt_client = mqtt::mqtt_client().unwrap();

    FreeRtos::delay_ms(10000);

    println!("Hello, world!");

    let mut led = PinDriver::output(peripherals.pins.gpio7)?;

    loop {
        led.set_high()?;
        // println!("Pinging Google DNS");
        println!("Pinging Google DNS");
        match wifi::ping(ipv4::Ipv4Addr::new(192, 168, 1, 1)) {
            Ok(_) => println!("Ping successful"),
            Err(e) => println!("Ping failed: {}", e),
        }
        FreeRtos::delay_ms(4000);
        led.set_low()?;
        FreeRtos::delay_ms(1000);
    }
}