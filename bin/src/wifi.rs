use std::net::Ipv4Addr;
use std::thread;
use std::time::Duration;

use embedded_svc::ipv4;
use esp_idf_hal::peripheral;

use esp_idf_svc::ping;
use esp_idf_svc::{wifi::{EspWifi, WifiWait}, eventloop::EspSystemEventLoop, netif::{EspNetif, EspNetifWait}};
use embedded_svc::wifi::{Configuration, ClientConfiguration, Wifi, AuthMethod};
use log::info;

use anyhow::{bail, Result};

pub fn wifi(
    modem: impl peripheral::Peripheral<P = esp_idf_hal::modem::Modem> + 'static,
    sysloop: EspSystemEventLoop,
    ssid: &str,
    pass: &str,
) -> Result<Box<EspWifi<'static>>> {

    let mut wifi = Box::new(EspWifi::new(modem, sysloop.clone(), None)?);

    info!("Wifi created, about to scan");

    let ap_infos = wifi.scan()?;

    let ours = ap_infos.into_iter().find(|a| a.ssid == ssid);

    let channel = if let Some(ours) = ours {
        info!(
            "Found configured access point {} on channel {}",
            ssid, ours.channel
        );
        Some(ours.channel)
    } else {
        info!(
            "Configured access point {} not found during scanning, will go with unknown channel",
            ssid
        );
        None
    };

    wifi.set_configuration(&Configuration::Client(
        ClientConfiguration {
            ssid: ssid.into(),
            password: pass.into(),
            auth_method: AuthMethod::WPA2Personal,
            channel,
            ..Default::default()
        }
    ))?;

    Ok(wifi)
}

pub fn start_wifi_controller_loop(wifi: Box<EspWifi<'static>>, sysloop: EspSystemEventLoop){

    thread::spawn(move || {
        let mut wifi = wifi;
        let sysloop = sysloop.clone();

        loop {
            if let Err(e) = start_and_wait(&mut wifi, &sysloop) {
                info!("Error while starting wifi: {}", e);
            } else {
                if let Err(e) = connect_and_wait(&mut wifi, &sysloop) {
                    info!("Error while connecting wifi: {}", e);
                } else {
                    info!("Wifi connected");
                    thread::sleep(Duration::from_secs(5));
                }
            }
            thread::sleep(Duration::from_secs(5));
        }
    });
}

fn connect_and_wait(wifi: &mut Box<EspWifi>, sysloop: &EspSystemEventLoop) -> Result<()> {

    if wifi.is_connected()? {
        return Ok(());
    }

    wifi.connect()?;

    if !EspNetifWait::new::<EspNetif>(wifi.sta_netif(), &sysloop)?.wait_with_timeout(
        Duration::from_secs(20),
        || {
            match wifi.is_connected() {
                Ok(true) => {
                    match wifi.sta_netif().get_ip_info() {
                        Ok(ip_info) => {
                            ip_info.ip != Ipv4Addr::new(0, 0, 0, 0)
                        },
                        Err(e) => {
                            info!("Error while waiting for wifi to connect: {}", e);
                            false
                        },
                    }
                },
                Ok(false) => false,
                Err(e) => {
                    info!("Error while waiting for wifi to connect: {}", e);
                    false
                },
            }
        },
    ) {
        bail!("Wifi did not connect or did not receive a DHCP lease");
    } else {
        Ok(())
    }
}

pub fn start_and_wait(wifi: &mut Box<EspWifi>, sysloop: &EspSystemEventLoop) -> Result<()> {
    if wifi.is_started()? {
        return Ok(());
    }

    wifi.start()?;

    if !WifiWait::new(&sysloop)?
        .wait_with_timeout(Duration::from_secs(20), || {
            match wifi.is_started() {
                Ok(true) => true,
                Ok(false) => false,
                Err(e) => {
                    info!("Error while waiting for wifi to start: {}", e);
                    false
                },
            }
        })
    {
        bail!("Wifi did not start");
    } else {
        Ok(())
    }

}

pub fn ping(ip: ipv4::Ipv4Addr) -> Result<()> {
    info!("About to do some pings for {:?}", ip);

    let ping_summary = ping::EspPing::default().ping(ip, &Default::default())?;
    if ping_summary.transmitted != ping_summary.received {
        bail!("Pinging IP {} resulted in timeouts", ip);
    }

    info!("Pinging done");

    Ok(())
}